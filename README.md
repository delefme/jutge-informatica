# Jutge Informàtica FME

Recull de programes del jutge de l'assignatura d'Informàtica de la FME, UPC.

## Hi són tots els problemes?

Sí... I no. Si creus que tens un problema que està més ben escrit o que s'entén millor que el què ja existeix, no dubtis en editar el repositori, per això és públic.

## Ús i raó de l'existència del repositori

Aquest repositori està pensat per complementar l'aprenentatge, no està pensat per substituïr res. També per reforçar l'estudi des de casa ara que la presencialitat està en dubte. És part del projecte de la "Comi Apunts" de la Delegació d'Estudiants de la FME.

## Agraïments

Voldríem agraïr a les següents persones per col·laborar amb la iniciativa:
- Ferran Hernandez Caralt
- Tomàs Gaeda Alcaide